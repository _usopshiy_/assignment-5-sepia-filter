%macro to_float 2
    xor rax, rax
    mov al, byte %2
    sar rax, 7
    mov al, byte %2
    movd %1, eax
    shufps %1, %1, 0
    cvtdq2ps %1, %1

%endmacro

section .rodata
align 16
f_line: dd 0.131, 0.168, 0.189
align 16
s_line: dd 0.534, 0.686, 0.769
align 16
t_line: dd 0.272, 0.346, 0.387

section .text
;rdi - pixel*
global sepia_filter_simd
sepia_filter_simd:

    ;put coefficents to xmm registers
    movups xmm3, [f_line]
    movups xmm4, [s_line]
    movups xmm5, [t_line]

    mov rsi, [rdi + 3]

    ;put rgb in xmm registers
    to_float xmm0, [rdi]
    to_float xmm1, [rdi + 1]
    to_float xmm2, [rdi + 2]

    ;process
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    ;sat
    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    ;save
    movd [rdi], xmm0
    mov [rdi + 3], rsi
    ret

