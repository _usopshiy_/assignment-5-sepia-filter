#pragma once

enum status {
    NOT_ENOUGH_ARGS,
    FILE_OPEN_ERROR,
    FILE_CLOSE_ERROR,
    BMP_HEADER_READ_ERROR,
    BMP_IMAGE_READ_ERROR,
    INVALID_BMP,
    BMP_HEADER_WRITE_ERROR,
    BMP_IMAGE_WRITE_ERROR,
    UNSUPPORTED_ANGLE,
    OUT_OF_MEMORY,
    UNSUPPORTED_FILTER,
    OK
};

enum open_file_options {
    READ,
    WRITE,
    APPEND,
    READ_BINARY,
    WRITE_BINARY,
    APPEND_BINARY,
    READ_WRITE,
    CREATE_READ_WRITE,
    APPEND_CREATE_READ_WRITE,
    READ_WRITE_BINARY,
    CREATE_READ_WRITE_BINARY,
    APPEND_CREATE_READ_WRITE_BINARY,
    READ_TEXT,
    WRITE_TEXT,
    APPEND_TEXT,
    READ_WRITE_TEXT,
    CREATE_READ_WRITE_TEXT,
    APPEND_CREATE_READ_WRITE_TEXT
};

static enum filters{
    SEPIA,
    SEPIA_SIMD
};
