#pragma once

#include "enums.h"
#include "image.h"
#include <stdlib.h>

enum status apply_filter(struct image* img, enum filters filter);
