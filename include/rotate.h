#pragma once

#include "enums.h"
#include "image.h"


enum status rotate_image(struct image* img, int32_t angle);
