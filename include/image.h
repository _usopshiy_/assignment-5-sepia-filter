#pragma once

#include <stdint.h>

#pragma pack(push, 1)

struct pixel { uint8_t b, g, r; };

#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel* data;
};


struct image create_image(const uint64_t width, const uint64_t height);
void free_image(struct image* img);
