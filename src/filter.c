#include "../include/filter.h"

extern void sepia_filter_simd(struct pixel* pix);


static uint8_t sat(uint16_t val){return val < 256 ? val : 255;}

static void sepia_filter(struct pixel* pix){
    const struct pixel old_pixel = *pix;
    pix->r = sat(old_pixel.r*0.393 + old_pixel.g*0.769 + old_pixel.b*0.189);
    pix->g = sat(old_pixel.r*0.349 + old_pixel.g*0.686 + old_pixel.b*0.168);
    pix->b = sat(old_pixel.r*0.272 + old_pixel.g*0.534 + old_pixel.b*0.131);
}

static void apply_to_pixels(struct image* img, void (func)(struct pixel*)){
    for(size_t i = 0; i < img->height * img->width; i++){
        func(&(img->data[i]));
    }
}


enum status apply_filter(struct image* img, enum filters filter){
    switch(filter){
        case SEPIA:
            apply_to_pixels(img, sepia_filter);
            return OK;
        case SEPIA_SIMD:
            apply_to_pixels(img, sepia_filter_simd);
            return OK;
        default:
            return UNSUPPORTED_FILTER;
    }
}