#include "../include/rotate.h"
#include <stdio.h>

static int32_t prepare_angle(int32_t angle){ //cast negative angles to their positive counterpart
    return((angle + 360) % 360);
}

static struct image rotate90(const struct image img){
    struct image rotated_image = create_image(img.height, img.width);
    if(!rotated_image.data){return rotated_image;}

    for(uint64_t y = 0; y < img.height; y++){
        for(uint64_t x = 0; x < img.width; x++){
            rotated_image.data[img.height*(img.width - x - 1) + y] = img.data[(y*img.width) + x];
        }
    }

    return rotated_image;
}


static struct image rotate180(const struct image img){
    struct image rotated_image = create_image(img.width, img.height);
    if(!rotated_image.data){return rotated_image;}

    for(uint64_t y = 0; y < img.height; y++){
        for(uint64_t x = 0; x < img.width; x++){
            rotated_image.data[(img.width*(img.height - y - 1)) + (img.width - x - 1)] = img.data[(y*img.width) + x];
        }
    }

    return rotated_image;
}


static struct image rotate270(const struct image img){
    struct image rotated_image = create_image(img.height, img.width);
    if(!rotated_image.data){return rotated_image;}

    for(uint64_t y = 0; y < img.height; y++){
        for(uint64_t x = 0; x < img.width; x++){
            rotated_image.data[((x)*img.height) +  (img.height - y - 1)] = img.data[(y*img.width) + x];
        }
    }

    return rotated_image;
}

enum status rotate_image(struct image* img, int32_t angle){
    if(angle == 0){return OK;}

    const int32_t prepared_angle = prepare_angle(angle);
    
    struct image rotated_image;
    switch (prepared_angle)
    {
    case 90:
        rotated_image = rotate90(*img);
        break;
    
    case 180:
        rotated_image = rotate180(*img);
        break;
    
    case 270:
        rotated_image = rotate270(*img);
        break;
    
    default:
        return UNSUPPORTED_ANGLE;
    }
    if(!rotated_image.data){return OUT_OF_MEMORY;}
    free_image(img);
    *img=rotated_image;
    return OK;
}
