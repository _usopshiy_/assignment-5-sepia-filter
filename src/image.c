#include "../include/image.h"
#include <malloc.h>


struct image create_image(const uint64_t width,const uint64_t height){
    if(width == 0 || height == 0){return (struct image) { 0 };}
    return( (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    });
}

void free_image(struct image* img){
    if(img->data){
        free(img->data);
    }
    img->data = NULL;
}
