#include "../include/error_manager.h"
#include "../include/filter.h"
#include "../include/image.h"
#include "../include/io_manager.h"
#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const uint8_t REQUIRED_ARGS_COUNT = 4;
const uint8_t INPUT_ARG = 1;
const uint8_t OUTPUT_ARG = 2;
const uint8_t OUTPUT_SIMD_ARG = 3;


int main( int argc, char** argv ) {
    if(argc != REQUIRED_ARGS_COUNT){throw_error(NOT_ENOUGH_ARGS); return 0;}

    char* input_file_path = argv[INPUT_ARG];
    char* output_file_path = argv[OUTPUT_ARG];
    char* output_simd_file_path = argv[OUTPUT_SIMD_ARG];

    enum status program_status;
    float c_time = 0;
    float asm_time = 0;

    //checking C
    struct image img = {.width = 0, .height = 0, .data = NULL};

    program_status = read_bmp_image(input_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

    clock_t start_c = clock();
    program_status = apply_filter(&img, SEPIA);
    if( program_status != OK){throw_error(program_status); return 0;};
    clock_t end_c = clock();
    c_time = end_c - start_c;


    program_status = write_bmp_image(output_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

    


    img = (struct image) {.width = 0, .height = 0, .data = NULL};
    program_status = read_bmp_image(input_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

    clock_t start_asm = clock();
    program_status = apply_filter(&img, SEPIA_SIMD);
    if( program_status != OK){throw_error(program_status); return 0;};
    clock_t end_asm = clock();
    asm_time = end_asm - start_asm;

    program_status = write_bmp_image(output_simd_file_path, &img);
    if( program_status != OK){throw_error(program_status); return 0;};

    printf("C: %f \n", c_time);
    printf("ASM: %f \n", asm_time);
}
