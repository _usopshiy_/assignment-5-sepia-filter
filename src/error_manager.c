#include "../include/error_manager.h"
#include <stdio.h>
#include <stdlib.h>


static const char* errors[12] =  {
    [NOT_ENOUGH_ARGS] = "Not enough args given",
    [FILE_OPEN_ERROR] = "Can't open file",
    [FILE_CLOSE_ERROR] = "Something went wrong while closing the file",
    [BMP_HEADER_READ_ERROR] = "Couldn't read bmp header",
    [BMP_IMAGE_READ_ERROR] = "Couldn't read bmp image",
    [INVALID_BMP] = "Unsupported bmp format",
    [BMP_HEADER_WRITE_ERROR] = "Something went wrong while writing bmp header",
    [BMP_IMAGE_WRITE_ERROR] = "Something went wrong while writing bmp image",
    [UNSUPPORTED_ANGLE] = "This angle is not supported! supported angles: 0, 90, 180, 270, -90, -180, -270",
    [OUT_OF_MEMORY] = "Run out of memory!",
    [UNSUPPORTED_FILTER] = "This filter is not supported!",
    [OK] = ""
};


static void print_error(const char* err){
    fprintf(stderr, "%s", err);
}



void throw_error(const enum status code){
    if(code == OK){return;}
    print_error(errors[code]);
}
