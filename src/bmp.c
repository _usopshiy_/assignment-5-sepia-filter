#include "../include/bmp.h"
#include "../include/enums.h"
#include <malloc.h>
#include <stdint.h>


#define BMP_FILE_TYPE 0x4D42    
#define BMP_HEADER_SIZE 40
#define BMP_COLOR_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_UNIMPORTANT_OR_DEFAULT 0
#define MAX_PADDING_SIZE 4
#define PIXEL_SIZE sizeof(struct pixel)
#define HEADER_SIZE sizeof(struct bmp_header)

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

uint32_t calculate_padding(uint64_t img_width) {
    return (MAX_PADDING_SIZE - ((img_width * PIXEL_SIZE) % MAX_PADDING_SIZE)) % MAX_PADDING_SIZE;
}

struct pixel* calculate_pointer(struct pixel* ptr, uint64_t width, uint64_t cur_row){
    return (struct pixel*) ptr + width*cur_row;
}

static struct bmp_header generate_header(const struct image* img){
    return (struct bmp_header) {
        .bfType = BMP_FILE_TYPE,
        .bfileSize = HEADER_SIZE +(img->height * (img->width * PIXEL_SIZE + calculate_padding(img->width))),
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = BMP_COLOR_PLANES,
        .biBitCount = BMP_BIT_COUNT,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = (img->width * PIXEL_SIZE + calculate_padding(img->width))*img->height, 
        .biXPelsPerMeter = BMP_UNIMPORTANT_OR_DEFAULT,
        .biYPelsPerMeter = BMP_UNIMPORTANT_OR_DEFAULT,
        .biClrUsed = BMP_UNIMPORTANT_OR_DEFAULT,
        .biClrImportant = BMP_UNIMPORTANT_OR_DEFAULT
    };
}


static enum status validate_bmp_format(const struct bmp_header header){
    if(header.biPlanes != BMP_COLOR_PLANES){return INVALID_BMP;}
    if(header.biCompression != BMP_COMPRESSION){return INVALID_BMP;} 
    if(header.biBitCount != BMP_BIT_COUNT){return INVALID_BMP;} //unsuported colour format
    return OK;
}



enum status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    if(fread(&header, HEADER_SIZE, 1, in) < 1){return BMP_HEADER_READ_ERROR;}
    if(validate_bmp_format(header) != OK){return INVALID_BMP;}
    
    *img = create_image(header.biWidth, header.biHeight);
    if(!img->data){return OUT_OF_MEMORY;}
    
    const uint32_t padding = calculate_padding(img->width);

    //set file ptr to pixels area
    if(fseek(in, header.bOffBits, SEEK_SET)){free(img->data); return BMP_IMAGE_READ_ERROR;} //if we didnt't get to data part - we didn't read the image
    
    for(size_t i = 0; i < img->height; i++){ 
        if(fread(calculate_pointer(img->data, img->width, i), PIXEL_SIZE*img->width, 1, in) < 1) {free(img->data); return  BMP_IMAGE_READ_ERROR;} //try to read row of pixels
        if(fseek(in, padding, SEEK_CUR)) {free(img->data); return BMP_IMAGE_READ_ERROR;} //had to do it twice since fseek can return succes after fread fail
    }
    return OK;
}

enum status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = generate_header(img);
    if(fwrite(&header, sizeof(header), 1, out) < 1){return BMP_HEADER_WRITE_ERROR;}

    const uint32_t padding = calculate_padding(img->width);

    for (size_t i = 0; i < img->height; i++){
        if(fwrite(img->data + img->width*i, PIXEL_SIZE*img->width, 1, out) < 1){return BMP_IMAGE_WRITE_ERROR;}
        if(fseek(out, padding, SEEK_CUR)){return BMP_IMAGE_WRITE_ERROR;}
    }

    return OK;
}
